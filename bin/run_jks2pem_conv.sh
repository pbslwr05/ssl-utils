#!/bin/bash
##################################################
# History:                                       #
#     2020-05-29 pb Initial version              #
#                                                #
# Purpose: To convert JKS keystore to PEM        #
#                                                #
# Usage: run_jks2pem_conv.sh input.jks pwd.txt   #
##################################################

##################################################

SCRIPT_NAME=$(basename $0)
RUNDATE=`date '+%Y%m%d%H%M%S'`
USR_HOME=`pwd`
JKS_INPUT=$1
PWD_FILE=$2
OUTDIR=${USR_HOME}/data
LOGFILE=${USR_HOME}/logs/${SCRIPT_NAME}_${RUNDATE}.log
P12_OUTPUT=${USR_HOME}/data/${JKS_INPUT%.*}.p12
PEM_OUTPUT=${USR_HOME}/data/${JKS_INPUT%.*}_PP.pem
PEM_NOPP=${USR_HOME}/data/${JKS_INPUT%.*}_NOPP.pem

if [ $# -eq 0 ]; then
    echo "No inputs provided"
    exit 1
fi

### Execute convertion from JKS to PEM
### First get store password from input file
STR_PWD=`cat $PWD_FILE |cut -d '' -f1`

### Now capture alias from source Keystore
ALIAS=`keytool -list -v -keystore $JKS_INPUT -storepass $STR_PWD | grep 'Alias name:'|cut -d ':' -f2`

### First convert JKS to P12 format
keytool -importkeystore -srckeystore $JKS_INPUT -destkeystore $P12_OUTPUT -srcalias $ALIAS \
        -srcstoretype jks -deststoretype pkcs12 -srcstorepass $STR_PWD -storepass $STR_PWD -keypass $STR_PWD

## Now export the certs in PEM format by removing passwords
## First generate PEM export
openssl pkcs12 -in $P12_OUTPUT -out $PEM_OUTPUT -passin pass:$STR_PWD -passout pass:$STR_PWD

openssl rsa -in $PEM_OUTPUT -out $PEM_NOPP -passin pass:$STR_PWD
openssl x509 -in $PEM_OUTPUT >> $PEM_NOPP

exit 0
